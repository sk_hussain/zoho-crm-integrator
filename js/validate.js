// JavaScript Document
var processForm;
var randomNumber;
var captchaVal;
var getTimezon;


jQuery(document).ready(function($) {


    // Reset the random number when modal is shown
    $('.modal').on('shown.bs.modal', function (e) {
        randomNumber(1,5);
        //$('input[name="captcha"]').val('');
        $('input[name="captcha"]')[0].placeholder='Your Answer';

    })

    // Function to generate the random number
    randomNumber = function(min,max) {
        //var ran1 = Math.floor(Math.random() * 10);
        //var ran2 = Math.floor(Math.random() * 10);
        var ran1 = Math.floor(Math.random() * (max - min + 1)) + min;
        var ran2 = Math.floor(Math.random() * (max - min + 1)) + min;
        captchaSum = ran1 + ran2;
        //console.log(ran1 +'+'+ ran2 +'='+ captchaSum);
        $('.captch-value').text('What is ' + ran1 + ' + ' + ran2 + ' ? ');
    }



    // For the Form that will appear in page not in Modal
    randomNumber(1,5);
    $('input[name="captcha"]')[0].placeholder='Your Answer';


    // Process the form after submitting.
    processForm = function(form) {
        var captcha_val = $('input[name="captcha"]', form).val();
        console.log(captcha_val);
        //console.log(captcha_val);
        if (captcha_val == captchaSum) {
            return true;
        } else if (captcha_val == '') {
            $('input[name="captcha"]')[0].placeholder = 'Please Enter Your Answer';
            $("#captcha", form).focus();
            return false;
        }
        // If NO captcha is defined
        else if (captcha_val == undefined) {
            return true;
        } else {
            $('.error-info', form)
                .text("Wrong Answer, Please Try Again!!")
                .show()
                .fadeOut(3000);
            $("#captcha", form).focus();
            return false;
        }
    }



    getTimezon = function (){
        var xmlhttp = new XMLHttpRequest();
        xmlhttp.open("GET","https://aplynk.com/wp-content/plugins/zoho-crm-integrator/timezones.json", false);
        xmlhttp.send();
        if(xmlhttp.readyState == 4 && xmlhttp.status == 200)
        {
            var jsonData = xmlhttp.responseText;
            //console.log(jsonData);

            var jsobject = JSON.parse(jsonData);
            //console.log(jsobject.zones[2].value);

            var jsobject_len = jsobject.zones.length;
            //console.log(jsobject_len);

            for(var i=1; i<jsobject_len;i++){
                var listitems;
                var name = jsobject.zones[i].name;
                var value = jsobject.zones[i].value;
                listitems += '<option value=' + value + '>' + value + '</option>';
            }

            //console.log(listitems);
            $('.get_time_zone').append(listitems);
        }
    }

    getTimezon();

    $('.get_first_date_time').datetimepicker();
    $('.get_second_date_time').datetimepicker();


});
