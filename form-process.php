<?php
require_once('recaptchalib.php');
require('../../../wp-blog-header.php');
require_once('class.phpmailer.php');

$lead_source = $_POST['zoho_source'];
if(!isset($lead_source) || strlen($lead_source) <= 1)
  $lead_source =  $_POST['video_option'];

$inquiry_type = $_POST['inquiry_type'];

$inquiry_type = mysql_escape_string($inquiry_type);
$zoho_source=mysql_escape_string($lead_source);
$company=mysql_escape_string($_POST['company']);
$first_name=mysql_escape_string($_POST['first_name']);
$last_name=mysql_escape_string($_POST['last_name']);
$email=mysql_escape_string($_POST['email']);
$title=mysql_escape_string($_POST['title']);
$phone=mysql_escape_string($_POST['phone']);
$fax=mysql_escape_string($_POST['fax']);
$mobile=mysql_escape_string($_POST['mobile']);
$description=mysql_escape_string($_POST['description']);
$chkrecap=mysql_escape_string($_POST['recap']);
$sendemail=mysql_escape_string($_POST['sendemail']);
$leadcf = mysql_escape_string($inquiry_type);


// HOME PAGE NEW APP FORM=================================================================
$integrate_app_one = mysql_escape_string($_POST['integrate_app_one']);
$integrate_with_app_two = mysql_escape_string($_POST['integrate_with_app_two']);

// Custom Fields=====================================================================================================

// Single Solution Page
$get_app_name_from_url_single_solution = mysql_escape_string($_POST['get_app_name_from_url_single_solution']);
$dropdown_for_app2=mysql_escape_string($_POST['dropdown_for_app2']);
$get_second_new_app_name=mysql_escape_string($_POST['get_second_new_app_name']);

//Demo Request page
$get_first_date_time=mysql_escape_string($_POST['get_first_date_time']);
$get_second_date_time=mysql_escape_string($_POST['get_second_date_time']);
$get_time_zone=mysql_escape_string($_POST['get_time_zone']);
$apps_custom_field=mysql_escape_string($_POST['apps_custom_field']); // APP1
$erp_finance_custom_field=mysql_escape_string($_POST['erp_finance_custom_field']); //APP2

//Integrations page 2 APP
$get_app_name_from_url_2App=mysql_escape_string($_POST['get_app_name_from_url_2App']);



$form_data="";
$form_data='<FL val="Lead Source">'.$zoho_source.'</FL>';

if($company!="")
$form_data.='<FL val="Company">'.$company.'</FL>';

if($first_name!="")
$form_data.='<FL val="First Name">'.$first_name.'</FL>';

if($last_name!="")
$form_data.='<FL val="Last Name">'.$last_name.'</FL>';

if($email!="")
$form_data.='<FL val="Email">'.$email.'</FL>';

if($title!="")
$form_data.='<FL val="Title">'.$title.'</FL>';

if($phone!="")
$form_data.='<FL val="Phone">'.$phone.'</FL>';

if($fax!="")
$form_data.='<FL val="Fax">'.$fax.'</FL>';

if($mobile!="")
$form_data.='<FL val="Mobile">'.$mobile.'</FL>';

if($leadcf!="")
$form_data.='<FL val="Type of Inquiry">'.$leadcf.'</FL>';



//Custom Mpping 

//Integration 2 App PAGE ================================================

$get_title_of_integrations_two_app = $get_app_name_from_url_2App;
if($get_title_of_integrations_two_app!="")
$form_data.='<FL val="Integration URL">'.$get_title_of_integrations_two_app.'</FL>';

//Integration 2 App PAGE END ===========================================




// SOLUTION PAGE==============================================================

// Demo Request Form
$one_app_with_second_app_dropdown = $get_app_name_from_url_single_solution . '->' . $dropdown_for_app2;
if($dropdown_for_app2!="")
$form_data.='<FL val="Solution URL">'.$one_app_with_second_app_dropdown.'</FL>';

// Suggest New integrations
if($get_app_name_from_url_single_solution!="")
$form_data.='<FL val="Apps">'.$get_app_name_from_url_single_solution.'</FL>';
//App2
if($get_second_new_app_name!="")
$form_data.='<FL val="ERP-Finance">'.$get_second_new_app_name.'</FL>';

// SOLUTION PAGE FORM END==============================================================


//Date 1
if($get_first_date_time!="")
$form_data.='<FL val="First Preferred Time for Demo">'.$get_first_date_time. '(' .$get_time_zone. ')</FL>';
//Date 2
if($get_second_date_time!="")
$form_data.='<FL val="Second Preferred Time for Demo">'.$get_second_date_time.'</FL>';

//App1
if($apps_custom_field!="")
$form_data.='<FL val="Apps">'.$apps_custom_field.'</FL>';
//App2
if($erp_finance_custom_field!="")
$form_data.='<FL val="ERP-Finance">'.$erp_finance_custom_field.'</FL>';




// HOME PAGE===========================================

// Suggest New APP from 
if($integrate_app_one!="")
$form_data.='<FL val="Apps">'.$integrate_app_one.'</FL>';
//App2
if($integrate_with_app_two!="")
$form_data.='<FL val="ERP-Finance">'.$integrate_with_app_two.'</FL>';





// //Testing=====================================
// //$all_test_fields = $apps_custom_field. ' | ' .$erp_finance_custom_field. ' | ' .$get_first_date_time. ' | ' .$get_first_date_time. ' | ';

// $all_test_fields = $get_app_name_from_url. ' | ' .$dropdown_for_app2.  ' | ' .$get_second_new_app_name ;
// if($all_test_fields!="")
// $form_data.='<FL val="Description">'.$all_test_fields.'</FL>';
// //Testing END=================================


if($description!="")
$form_data.='<FL val="Description">'.$description.'</FL>';

$options = get_option('my_option_name' );
$authtoken=$options['authtoken'];

if($chkrecap=="yes")
{
$recapoptions = get_option('zoho_crm_integrator_recaptcha_options' );
$privatekey=$recapoptions['private_key'];
$resp = recaptcha_check_answer ($privatekey,
                                $_SERVER["REMOTE_ADDR"],
                                $_POST["recaptcha_challenge_field"],
                                $_POST["recaptcha_response_field"]);
}




if ((!$resp->is_valid)&&($chkrecap=="yes")) {
    // What happens when the CAPTCHA was entered incorrectly
    die ("fail");
  } else {
    // Your code here to handle a successful verification
if($sendemail=="yes"){
$emailoptions = get_option('zoho_crm_integrator_email_options' );
$sendemailid=$emailoptions['emailid'];
$subject=$emailoptions['subject'];

$mail             = new PHPMailer();

$body             = '<table width="500" border="0">';

if($company!="")
$body.='<tr><td width="155" >Company :</td><td width="15">&nbsp;</td><td width="316">'.$company.'</td></tr>';

if($first_name!="")
$body.='<tr><td width="155" >First name :</td><td width="15">&nbsp;</td><td width="316">'.$first_name.'</td></tr>';

if($last_name!="")
$body.='<tr><td width="155" >Last name :</td><td width="15">&nbsp;</td><td width="316">'.$last_name.'</td></tr>';

if($email!="")
$body.='<tr><td width="155" >E-mail :</td><td width="15">&nbsp;</td><td width="316">'.$email.'</td></tr>';

if($title!="")
$body.='<tr><td width="155" >Title :</td><td width="15">&nbsp;</td><td width="316">'.$title.'</td></tr>';

if($phone!="")
$body.='<tr><td width="155" >Phone :</td><td width="15">&nbsp;</td><td width="316">'.$phone.'</td></tr>';

if($fax!="")
$body.='<tr><td width="155" >Fax :</td><td width="15">&nbsp;</td><td width="316">'.$fax.'</td></tr>';

if($mobile!="")
$body.='<tr><td width="155" >Mobile :</td><td width="15">&nbsp;</td><td width="316">'.$mobile.'</td></tr>';

if($description!="")
$body.='<tr><td width="155" >Message :</td><td width="15">&nbsp;</td><td width="316">'.$description.'</td></tr>';


$body.='</table>';


$mail->SetFrom($email, $first_name.' '.$last_name);

$mail->AddReplyTo($email,$first_name.' '.$last_name);

$mail->Subject    = $subject;

$mail->AltBody    = "To view the message, please use an HTML compatible email viewer!"; // optional, comment out and test

$mail->MsgHTML($body);


$mail->AddAddress($sendemailid);


if(!$mail->Send()) {
 // echo "Mailer Error: " . $mail->ErrorInfo;
} else {
 // echo "Message sent!";
}
}



$url = 'https://crm.zoho.com/crm/private/xml/Leads/insertRecords';
$xmldata='<Leads>
<row no="1">
'.$form_data.'
</row>
</Leads>';

$fields = array(
            'newFormat'=>1,
            'authtoken'=>$authtoken,
            'scope'=>'crmapi',
			'xmlData'=>$xmldata,
            'wfTrigger' => 'true'
        );
$fields_string = NULL;
foreach($fields as $key=>$value) { $fields_string .= $key.'='.$value.'&'; }
$fields_string = rtrim($fields_string,'&');


$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
curl_setopt($ch, CURLOPT_POST,1);
curl_setopt($ch, CURLOPT_POSTFIELDS,$fields_string);
curl_setopt($ch, CURLOPT_FOLLOWLOCATION  ,1);
curl_setopt($ch, CURLOPT_HEADER      ,0);  // DO NOT RETURN HTTP HEADERS
curl_setopt($ch, CURLOPT_RETURNTRANSFER  ,1);  // RETURN THE CONTENTS OF THE CALL


curl_exec($ch);

curl_close($ch);
    
}

get_header(); ?>

<div class="text-center" style="padding:10% 0">
<h1>Thanks for your interest in Aplynk.</h1>
<h3>Someone from our team will get in touch with you soon.</h3>
   
</div>



<?php get_footer();
?>
